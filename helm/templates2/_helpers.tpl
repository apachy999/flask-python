{{/* vim: set filetype=mustache: */}}
{{/*
Expand the name of the chart.
*/}}
{{- define "helm.name" -}}
{{- default .Chart.Name .Values.nameOverride | trunc 63 | trimSuffix "-" -}}
{{- end -}}

{{/*
Create chart name and version as used by the chart label.
*/}}
{{- define "helm.chart" -}}
{{- printf "%s-%s" .Chart.Name .Chart.Version | replace "+" "_" | trunc 63 | trimSuffix "-" -}}
{{- end -}}

{{/*
Common labels
*/}}
{{- define "chart.ingressBasicAnotations" -}}
kubernetes.io/ingress.class: nginx
{{- if .Values.ingress.authSecret }}
nginx.ingress.kubernetes.io/auth-realm: Authentication Required
nginx.ingress.kubernetes.io/auth-secret: {{ .Values.ingress.authSecret }}
nginx.ingress.kubernetes.io/auth-type: basic
{{- end -}}
{{- if .Values.ingress.prometheus }}
prometheus.io/probe: "true"
prometheus.io/healthroute: {{ .Values.ingress.healthroute }}
{{- else }}
prometheus.io/probe: "true"
{{- end -}}
{{- end -}}
{{- define "helm.labels" -}}
helm.sh/chart: {{ include "helm.chart" . }}
{{ include "helm.selectorLabels" . }}
{{- if .Chart.AppVersion }}
app.kubernetes.io/version: {{ .Chart.AppVersion | quote }}
{{- end }}
app.kubernetes.io/managed-by: {{ .Release.Service }}
{{- range $key, $val := .Values.labels }}
{{ $key }}: {{ $val }}
{{- end }}
{{- end -}}

{{/*
Selector labels
*/}}
{{- define "helm.selectorLabels" -}}
app.kubernetes.io/name: {{ include "helm.name" . }}
app.kubernetes.io/instance: {{ .Release.Name }}
{{- end -}}



{{/*
Template to generate apiVersion for ingress
*/}}
{{- define "apiVersions.ingress" }}
{{- $apiVersion := "" }}
{{- $gitVersion := $.Capabilities.KubeVersion.GitVersion -}}
{{- $apiVersions := $.Capabilities.APIVersions -}}
{{- if and ($apiVersions.Has "networking.k8s.io/v1") (semverCompare ">= 1.19.x" $gitVersion) -}}
{{- print "networking.k8s.io/v1" -}}
{{- else -}}
{{- print "networking.k8s.io/v1beta1" -}}
{{- end -}}
{{- end }}
