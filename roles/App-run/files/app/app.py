 #!/usr/bin/env python
 # -*- coding: utf-8 -*-

from flask import Flask
from prometheus_flask_exporter import PrometheusMetrics

app = Flask(__name__)
metrics = PrometheusMetrics(app)

# static information as metric
#metrics.info('app_info', 'Application info', version='1.0.3')
#counter1 = 0


#metrics.register_default(
#    metrics.counter(
#        'by_path_counter', 'Request count by request paths',
#        labels={'path': lambda: request.path}
#    )
#)

@app.route('/')
def main():
    import os
#    global counter1
#    counter1 += 1
    return "Hello " + os.environ['BRANCHNAME'] + " !"
#    return "This page was served " + str(counter) + " times. " "Hello " + os.environ['BRANCHNAME'] + " !"



#@app.route('/metrics')
#def metrics():
#    return "Main page was served " + str(counter) + " times. "

#if __name__ == "__main__":
#    app.run(debug=True,host='0.0.0.0')
if __name__ == "__main__":
    app.run("0.0.0.0", 5000, threaded=True)
