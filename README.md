# Flask-Python



## Getting started

```
mkdir flask-python-project
cd flask-python-project
virtualenv venv
sudo apt install python3-virtualenv
virtualenv venv
source venv/bin/activate
pip install flask
mkdir my_flask_app
pip freeze > my_flask_app/requirements.txt
nano my_flask_app/app.py
```

```mermaid
graph LR;
  untracked -- "git add" --> staged;
  staged    -- "???"     --> tracked/comitted;

%% стрелка без текста для примера:
  A --> B;
``` 
